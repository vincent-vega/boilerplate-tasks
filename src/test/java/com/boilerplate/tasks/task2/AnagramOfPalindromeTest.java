package com.boilerplate.tasks.task2;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author Wojciech Koszycki
 */
public class AnagramOfPalindromeTest {

    public AnagramOfPalindromeTest() {
    }

    @Test
    public void testSimpleWord() {
        String anagram = shuffleString("kayak");
        boolean result = new AnagramOfPalindrome().isAnagramPalindrome(anagram);
        assertTrue(result);
    }

    @Test
    public void testSentence() {
        String anagram = shuffleString("RaceFastSafeCar");
        boolean result = new AnagramOfPalindrome().isAnagramPalindrome(anagram);
        assertTrue(result);
    }

    @Test
    public void testNonAnagram() {
        String anagram = shuffleString("aabbcdccbbaa");
        boolean result = new AnagramOfPalindrome().isAnagramPalindrome(anagram);
        assertFalse(result);
    }

    public static String shuffleString(String string) {
        List<String> letters = Arrays.asList(string.split(""));
        Collections.shuffle(letters);
        StringBuilder sb = new StringBuilder();
        for (String letter : letters) {
            sb.append(letter);
        }
        return sb.toString();
    }
}
